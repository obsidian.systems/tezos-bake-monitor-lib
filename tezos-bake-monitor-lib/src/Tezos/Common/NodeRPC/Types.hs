{-# LANGUAGE CPP #-}
{-# LANGUAGE TemplateHaskell #-}

module Tezos.Common.NodeRPC.Types where

import Control.Lens (Prism', re, (^.))
import Control.Lens.TH (makePrisms)
#if !(MIN_VERSION_base(4,9,0))
import Data.Semigroup
#endif
import Control.Exception.Safe (Exception)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import Data.Sequence (Seq)
import Data.Text (Text)
import Data.Typeable (Typeable)

import Tezos.Common.Json (JsonRpcError)

type RpcResponse = Either RpcError
data RpcError
  = RpcError_HttpException Text
  | RpcError_UnexpectedStatus Int BS.ByteString
  | RpcError_NonJSON String LBS.ByteString
  | RpcError_Rejected (Seq JsonRpcError)
  deriving (Eq, Ord, Show, Typeable)
instance Exception RpcError

makePrisms ''RpcError

class AsRpcError e where
  asRpcError :: Prism' e RpcError

instance AsRpcError RpcError where
  asRpcError = id

rpcResponse_HttpException :: (AsRpcError e) => Text -> e
rpcResponse_HttpException x = RpcError_HttpException x ^. re asRpcError

rpcResponse_UnexpectedStatus :: (AsRpcError e) => Int -> BS.ByteString -> e
rpcResponse_UnexpectedStatus x y = RpcError_UnexpectedStatus x y ^. re asRpcError

rpcResponse_NonJSON :: (AsRpcError e) => String -> LBS.ByteString -> e
rpcResponse_NonJSON x y = RpcError_NonJSON x y ^. re asRpcError

rpcResponse_Rejected :: (AsRpcError e) => Seq JsonRpcError -> e
rpcResponse_Rejected xs = RpcError_Rejected xs ^. re asRpcError
